<?php


namespace App\Lib;

require('xmlreader-iterators.php');

class XmlIterator extends \XMLElementIterator
{

    public function __construct(\XMLReader $reader, $elementName = "")
    {
        parent::__construct($reader, $elementName);
    }

    /**
     * @return SimpleXMLElement
     */
    public function current()
    {
        return simplexml_load_string($this->reader->readOuterXml());
    }
}