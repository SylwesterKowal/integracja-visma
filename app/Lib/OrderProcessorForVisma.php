<?php

namespace App\Lib;

use Illuminate\Support\Facades\Log;
use App\Lib\CurlServices;
use Kowal\LumenVisma\Client;
use Kowal\LumenVisma\Entities\Account;
use Illuminate\Support\Facades\DB;
use Kowal\LumenVisma\Entities\CustomerInvoices;
use Kowal\LumenVisma\Entities\Customers;
use Kowal\LumenVisma\Entities\CustomerInvoiceRow;
use Kowal\LumenVisma\Entities\Articles;
use Kowal\LumenVisma\Entities\ArticleAccountCodings;
use Kowal\LumenVisma\Entities\Units;
use Kowal\LumenVisma\Entities\TermsOfPayment;

class OrderProcessorForVisma
{
    public function execute($data)
    {
        /** @var Client $client */
        $client = app()->make(Client::class)->connect();
        $client->setToken(Tokens::getAccessToken());

//        $termsOfPayment = (new TermsOfPayment($client))->index();
        $termsOfPaymentId = 'cefd51c4-ba4d-449d-8ef3-fe427dfd3099'; // Card payment

        // Log::debug(print_r($termsOfPayment,true));

        $invoice = new CustomerInvoices($client);
        $invoice->EuThirdParty = true;
        $invoice->InvoiceDate = date("Y-m-d", strtotime((string)$data['order']->created_at));
        $invoice->DueDate = date("Y-m-d", strtotime((string)$data['order']->created_at));
        $invoice->CustomerId = $this->createCustomer($client, $data, $termsOfPaymentId);
        $invoice->Rows = $this->createRows($client, $data);
        $invoice->RotReducedInvoicingType = 0;
        $invoice->YourReference = (string)$data['order']->increment_id;

        $result = $invoice->save();
        Log::debug(print_r($result, true));
        return isset($result->Id) ? $result->Id : false;
    }

    private function createCustomer($client, $data, $termsOfPaymentId)
    {
        $customer = new Customers($client);
        $customer->InvoiceCity = (string)$data['order']->billing_address->city;
        $customer->InvoicePostalCode = (string)$data['order']->billing_address->postcode;
        $customer->Name = (string)$data['order']->billing_address->firstname . ' ' . (string)$data['order']->billing_address->lastname;
        $customer->TermsOfPaymentId = $termsOfPaymentId;
        $customer->IsPrivatePerson = true;
        $customer->IsActive = true;

        $result = $customer->save();
        //Log::debug(print_r($result, true));
        return (string)$result->Id;
    }

    private function createRows($client, $data)
    {


        $rows = [];

        $i = 1;
        foreach ($data['order']->items as $item) {
            $row = new CustomerInvoiceRow($client);
            $row->ArticleId = $this->getOrSetArticle($item, $client);
            $row->IsServiceArticle = false;
            $row->LineNumber = $i;
            $row->PercentVat = (integer)$item->tax_percent;
            $row->Quantity = (integer)$item->qty_ordered;
            $row->UnitPrice = round((float)$item->price_incl_tax, 2);
            $rows[] = $row;
            $i++;
        }

        if ($data['order']->shipping_amount > 0) {
            $row = new CustomerInvoiceRow($client);
            $itemShip = new \stdClass();
            $itemShip->sku = 'SHIPPING';
            $itemShip->name = 'Shipping';

            $row->ArticleId = $this->getOrSetArticle($itemShip, $client, true);
            $row->IsServiceArticle = true;
            $row->LineNumber = $i;
            $row->PercentVat = 25;
            $row->Quantity = 1;
            $row->UnitPrice = $this->getShippingValue($data);
            $rows[] = $row;
        }
        return $rows;
    }

    private function getShippingValue($data)
    {
        $shippingValue = 0;
        foreach ($data['order']->extension_attributes->shipping_assignments as $shipping) {
            $shippingValue = $shippingValue + (float)$shipping->shipping->total->shipping_incl_tax;
        }
        return round($shippingValue, 2);
    }


    private function getOrSetArticle($item, $client, $shipping = false)
    {
        $articles = (new Articles($client))->index();
//        Log::debug(print_r($articles, true));
        $find = false;

        foreach ($articles as $article) {

            if ($article->Number == $item->sku) {
                $find = true;
                return (string)$article->Id;
            }
        }

        if (!$find) {

//            $articlesAccountCodings = (new ArticleAccountCodings($client))->index(date('Y-m-d'));
//            Log::debug(print_r($articlesAccountCodings, true));
//            $units = (new Units($client))->index();
//            Log::debug(print_r($units, true));

            $article = new Articles($client);

            $article->IsActive = "TRUE";
            $article->CodingId = ($shipping) ? "4e81d38d-25d6-4e10-8b8f-1d8593f1f811" : "6d5f8cd1-a265-41d6-bce0-47b54cb0e484"; // shipping:
            $article->Name = (string)$item->name;
            $article->Number = (string)$item->sku;
            $article->UnitId = '79c71fb0-3a3b-414e-80e2-a30a04b23295';
            $result = $article->save();
            return (string)$result->Id;
        }
    }
}