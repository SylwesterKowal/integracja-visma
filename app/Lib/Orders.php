<?php

namespace App\Lib;

use Illuminate\Support\Facades\Log;
use App\Lib\CurlServices;

class Orders
{
    private $host;
    private $protocol;
    private $access_token;
    private $store_code;

    private $curlServices;


    public function __construct(
        CurlServices $curlServices
    )
    {
        $this->curlServices = $curlServices;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }


    /**
     * @return array
     */
    public function getOrdersByStatus($status = 'processing')
    {
        $data = [];
        return $this->curl('/index.php/rest/' . $this->store_code . '/V1/orders?searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' . $status, 'GET', $data);
    }

    public function getProductBySku($sku = "")
    {
        $data = [];
        return $this->curl('/rest/' . $this->store_code . '/V1/products/' . $sku, 'GET', $data);
    }


    public function setOrderStatus($orderID = null, $entityId, $status = "completed")
    {
        $data = [
            "entity" => [
                "entity_id" => $entityId,
                "status" => $status,
                "increment_id" => $orderID
            ]];
        Log::debug("DATA: " .print_r( $data,true));
        return $this->curl('/index.php/rest/' . $this->store_code . '/V1/orders', 'POST', $data);
    }

    public function curl($endpoint, $request = 'GET', $data = [])
    {
        return $this->curlServices
            ->setUrl($this->host, $this->protocol)
            ->setCreditials($this->access_token)
            ->setRequest($request)
            ->setDataString($data)
            ->exexute($endpoint);
    }
}
