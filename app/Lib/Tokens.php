<?php

namespace App\Lib;

use Illuminate\Support\Facades\DB;

class Tokens
{
    public function getAuthorizationCode()
    {
        $result = DB::select("SELECT authorization_code FROM tokens WHERE id=1");
        return (isset($result[0]->authorization_code)) ? $result[0]->authorization_code : null;
    }

    public function getAccessToken()
    {
        $result = DB::select("SELECT access_token FROM tokens WHERE id=1");
        return (isset($result[0]->access_token)) ? $result[0]->access_token : null;
    }

    public function getRefreshToken()
    {
        $result = DB::select("SELECT refesh_token FROM tokens WHERE id=1");
        return (isset($result[0]->refesh_token)) ? $result[0]->refesh_token : null;
    }
}