<?php

namespace App\Lib;

use Illuminate\Support\Facades\Log;

class MyXmlReader
{

    private $xmlFile;
    public $xmlRender;

    public function setXmlFile($_xmlFile = '')
    {
        $this->xmlFile = $_xmlFile;
        return $this;
    }

    public function close()
    {
        $this->xmlRender->close();
    }

    public function exexute()
    {
        $this->xmlRender = new \XMLReader();
        $this->xmlRender->open($this->xmlFile);
    }

}
