<?php
namespace App\Lib;

use Illuminate\Support\Facades\Log;

class CurlServices
{
    private $webApiUrl;
    private $dataString = [];
    private $requestType = "POST";
    private $adminName;
    private $adminPassword;
    private $accessToken;

    /**
     * https://kowal.co/index.php/rest
     *
     * @param $url
     * @return $this
     */
    public function setUrl($host, $protocol = 'https')
    {
        $this->webApiUrl = $protocol . '://' . $host;
        return $this;
    }

    /**
     * @param string $dataString
     * @return $this
     */
    public function setDataString($dataString = "")
    {
        $this->dataString = $dataString;
        return $this;
    }

    /**
     * @param string $requestType
     * @return $this
     */
    public function setRequest($requestType = "POST")
    {
        $this->requestType = $requestType;
        return $this;
    }

    /**
     * @param $adminName
     * @param $adminPassword
     * @return $this
     */
    public function setCreditials($adminPassword)
    {
        $this->adminPassword = $adminPassword;
        $this->accessToken = $adminPassword;
        return $this;
    }

    public function exexute($resource, $array = false)
    {
        if ($this->accessToken === null) {
            $this->requestToken();
        }

        $ch = curl_init($this->webApiUrl . $resource);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->requestType);
        if ($this->requestType != 'GET')
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->dataString));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $this->accessToken)
        );
        $result = curl_exec($ch);

        if ($result === false) {
            Log::debug("ERROR: " . print_r(curl_error($ch), true));
            $result_data = false;
        } else {
            $result_data = ($array) ? json_decode($result, true) : json_decode($result);
        }

        curl_close($ch);
        // file_put_contents("__url.txt",print_r($this->webApiUrl . $resource,true));

        return $result_data;
    }

    /**
     * @return mixed
     */
    public function __exexute($resource, &$httpCodeParam = NULL)
    {

        $url = "";
        $ch = curl_init();
        $jsonData = "";

        if ($this->accessToken === null) {
            // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Content-Type: application/json',
//                'Content-Length: ' . strlen($this->dataString))
//        );

            $curlOptions = [
                CURLOPT_CUSTOMREQUEST => $this->requestType,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Content-Length: ' . strlen(json_encode($this->dataString)))
            ];
        } else {
            $curlOptions = [
                CURLOPT_CUSTOMREQUEST => $this->requestType,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array("Content-Type: application/json", "Authorization: Bearer " . $this->accessToken)
            ];
        }
        if ($this->requestType == "GET") {
            if ($this->dataString) {
                $query = http_build_query($this->dataString);
                $url = $this->webApiUrl . $resource . "?" . $query;
//                Log::debug($url);
            } else {
                $url = $this->webApiUrl . $resource;
//                Log::debug($url);
            }
        } else {
            if ($this->dataString) {
                $jsonData = json_encode($this->dataString);
                if ($jsonData === false) {
                    throw new \Exception("Could not encode JSON code:" . json_last_error());
                }
                $curlOptions[CURLOPT_POSTFIELDS] = $jsonData;
            }
            $url = $this->webApiUrl . $resource;
        }
        $curlOptions[CURLOPT_URL] = $url;

//        Log::debug($curlOptions);
        curl_setopt_array($ch, $curlOptions);

        $response = curl_exec($ch);
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_error($ch)) {
            $errno = curl_errno($ch);
            $error = curl_error($ch);
            throw new \Exception("$httpcode $this->requestType $url : curl error $errno: $error");
        }

        if (strpos($contentType, "application/json") > -1) {
            $response = json_decode($response);
        } else {
            throw new \Exception("Expected json: $this->requestType $url response:\n " . $response->message);
        }


        curl_close($ch);

        $httpCodeParam = $httpcode;
        switch ($httpcode) {
            case 200: /* OK */
                return $response;
                break;
            case 401: // Unauthorized
                throw new \Exception("Unauthorized:\n " . $response->message);
                break;
            case 404: /* Not found */
                return $response;
                break;
            case 400: /* Bad request */
                return $response;
                break;
            default:
                $msg = "$httpcode - $this->requestType $url data:\n" . print_r($this->dataString, true) . "\n response:\n" . $response->message;
                throw new \Exception($msg);
                break;
        }
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function requestToken()
    {

        $this->accessToken = $this->adminPassword;
        return;

        $data = [
            "username" => $this->adminName,
            "password" => $this->adminPassword
        ];

        $ch = curl_init($this->webApiUrl . '/rest/V1/integration/admin/token/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(json_encode($data)))
        );
        $result = curl_exec($ch);
        curl_close($ch);
        $this->accessToken = json_decode($result);
    }


    /**
     * @throws \Exception
     */
    public function checkAccessFields()
    {
        if (($this->webApiUrl === null || $this->adminName === null || $this->adminPassword === null)) {
            throw new \Exception("MagentoRESTService: missing username/password/webapiurl, use setAccessFields()");
        }
//        if ($this->accessToken === null) $this->requestToken();

        return $this;
    }


}
