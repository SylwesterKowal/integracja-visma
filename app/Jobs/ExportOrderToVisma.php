<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;
use App\Lib\OrderProcessorForVisma;
use App\Lib\Orders;
use App\Lib\CurlServices;

class ExportOrderToVisma extends Job
{
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data )
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrderProcessorForVisma $orderProcessorForVisma)
    {
        Log::debug("START ORDER JOB: " . $this->data['order']->increment_id);
        if ($vismaInvoiceId = $orderProcessorForVisma->execute($this->data)) {
            // set Order status Completed
            $orders = new \App\Lib\Orders(new CurlServices());
            $orders
                ->__set('host', $this->data['magento']['host'])
                ->__set('protocol', $this->data['magento']['protocol'])
                ->__set('access_token', $this->data['magento']['access_token'])
                ->__set('store_code', $this->data['magento']['store_code'])
                ->setOrderStatus($this->data['order']->increment_id, $this->data['order']->entity_id);
        }
//        Log::debug("END ORDER JOB: " .print_r( $this->data,true));

        return true;
    }
}
