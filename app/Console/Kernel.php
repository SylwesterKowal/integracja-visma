<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use MongoDB\Driver\Command;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AuthenticationLink::class,
        Commands\ExportOrders::class,
        Commands\AccessToken::class,
        Commands\RefreshToken::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('queue:work --queue=important,high,default --timeout=0 --stop-when-empty --tries=3')->everyMinute()->withoutOverlapping();
        $schedule->command('visma:refresh')->twiceDaily(3,16)->withoutOverlapping();
    }
}
