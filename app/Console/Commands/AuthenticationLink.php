<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-15
 * Time: 15:24
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Kowal\LumenVisma\Client;

class AuthenticationLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visma:link';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read all processing orders';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /** @var Client $client */
        $client = app()->make(Client::class)->connect();
        $client->setState(rand(10000, 99999));
        //echo $url = $client->getAuthorizationUrl();
         echo 'CLICK IT: '.$client->getUrlAuthorize().'?client_id='.config('visma.client_id').'&redirect_uri='.config('visma.redirect_uri').'&scope=ea:api+offline_access+ea:sales&response_type=code';
    }



}
