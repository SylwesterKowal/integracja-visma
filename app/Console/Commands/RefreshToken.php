<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-15
 * Time: 15:24
 */

namespace App\Console\Commands;

use App\Lib\Tokens;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Kowal\LumenVisma\Client;
use Illuminate\Support\Facades\DB;


class RefreshToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visma:refresh';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read all processing orders';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $refeshToken = Tokens::getRefreshToken();

        /** @var Client $client */
        $client = app()->make(Client::class)->connect();

        /** @var AccessTokenInterface $tokens */
        $tokens = $client->getNewRefreshToken($refeshToken);

        // Store those for future requests
        $accessToken = $tokens->getToken();
        $refeshToken = $tokens->getRefreshToken();
        $affected = DB::update(
            'update tokens set access_token = ?, refesh_token = ? where id = 1',
            [$accessToken, $refeshToken]
        );
        Log::debug(print_r($accessToken, true));
    }


}
