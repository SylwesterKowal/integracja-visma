<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-15
 * Time: 15:24
 */

namespace App\Console\Commands;

use App\Jobs\ExportOrderToVisma;
use App\Lib\Orders;
use App\Lib\MyXmlReader;
use App\Lib\XmlIterator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Kowal\LumenVisma\Client;

class ExportOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visma:export {order?}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read all processing orders';

    protected $host;
    protected $protocol;
    protected $access_token;
    protected $order_status;
    protected $store_code;

    public function __construct(
        MyXmlReader $myXmlReader,
        Orders      $orders
    )
    {
        $this->myXmlReader = $myXmlReader;
        $this->orders = $orders;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $test_order = $this->argument('order');
        Log::debug("TRYB TESTOWY ORDER: " . $test_order);

        if ($integracje = $this->getIntegracje()) {

            foreach ($integracje as $integracja) {
                if ((string)$this->myXmlReader->xmlRender->getAttribute('status') != 'enable') continue;
                $this->getSourceInfo($integracja);
                $orders = $this->orders
                    ->__set('host', $this->host)
                    ->__set('protocol', $this->protocol)
                    ->__set('access_token', $this->access_token)
                    ->__set('store_code', $this->store_code)
                    ->getOrdersByStatus($this->order_status);

//                Log::debug(print_r($orders,true));

                if (isset($orders->items)) {
                    foreach ($orders->items as $order) {

                        if (!empty($test_order) && $order->increment_id != $test_order) continue;


                        Log::debug(print_r($order,true));

                        $data = [
                            'order' => $order,
                            'magento' => [
                                'host' => $this->host,
                                'protocol' => $this->protocol,
                                'access_token' => $this->access_token,
                                'store_code' => $this->store_code
                            ]];

                        Queue::push(new ExportOrderToVisma($data ), '', 'high');

                    }
                }


            }
        }
        Log::debug("KONIEC");
    }


    public function getIntegracje()
    {
        $this->myXmlReader
            ->setXmlFile(base_path() . '/' . env('INTEGRACJE'))
            ->exexute();

        return new XmlIterator($this->myXmlReader->xmlRender, 'item');

    }

    private function getSourceInfo($integrcja)
    {
        $this->host = (string)$integrcja->host;
        $this->protocol = (string)$integrcja->protocol;
        $this->access_token = (string)$integrcja->access_token;
        $this->order_status = (string)$integrcja->order_status;
        $this->store_code = (string)$integrcja->store_code;
    }
}
